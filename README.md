# PierwszyProjekt

1. Pobierz repozytorium
   git clone https://malejadam@bitbucket.org/malejadam/pierwszyprojekt.git
2. Uruchom pobrane repozytorium za pomocą Visual Studio Code
3. W pliku dialer.js w linii 12 (obiekt config) wprowadź swój login.
   Następnie wprowadź hasło
4. W konsoli (skrót ctrl + shift + `) wpisz odpowiednio polecenia:
   npm install
   node dialer.js
5. Uruchom przeglądarkę wprowadź adres:
   localhost:3000/call/<numer1>/<numer2>

